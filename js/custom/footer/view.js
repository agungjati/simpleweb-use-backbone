define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./../js/custom/footer/template.html');
    var Cookies = require('Cookies');

    module.exports = LayoutManager.extend({
        tagName: 'nav',
        className: 'navbar fixed-bottom',
        attributes: {
            style: 'background:#fff; box-shadow:0 2px 3px #ccc'
        },
        template: _.template(template),
        afterRender: function() {
            var role = Cookies.get("roleName");
            var home = $("#nav a")[0];
            if (role == "Candidate")
                this.$(`a[name="home"]`).attr('href', '#jobAll');
            else
                this.$(`a[name="home"]`).attr('href', '#job');
        }
    });
});