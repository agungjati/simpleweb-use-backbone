define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var Model = require('./../js/custom/header/model');
    var template = require('text!./../js/custom/header/template.html');

    module.exports = LayoutManager.extend({
        tagName: 'nav',
        className: 'navbar fixed-top mb-4',
        attributes: {
            style: 'background:#fff;box-shadow: 0 2px 3px #ccc',

        },
        template: _.template(template),
        initialize: function() {
            this.model = new Model();
            this.listenTo(this.model, 'sync', function() {
                window.location = '#';
            });
        },
        events: {
            'click a[name="goback"]': 'goback',
            'click a[name="logout"]': 'logout'
        },
        goback: function() {
            window.history.back();
        },
        logout: function() {
            this.model.save();
        }
    });
});