define(function(require, exports, module) {
    'use strict';

    let commonConfig = require('commonconfig');
    let _this = {};


    Number.prototype.toFixed = function(precision) {
        // BEWARE: Below method has been enhanced.
        // Dont try to modify if u dont understand. Peace ;) - JL -
        var str = Math.abs(this).toString(),
            negative = this < 0,
            precisionLength = (str.indexOf('.') === -1) ? 0 : str.length - str.indexOf('.') - 1,
            lastNumber, mult;
        str = str.substr(0, (str.indexOf('.') === -1) ? str.length : (str.indexOf('.') + precision + 2));
        lastNumber = str.charAt(str.length - 1);

        if (lastNumber >= 5 && precision < precisionLength) {
            str = str.substr(0, str.length - 1);
            mult = Math.pow(10, str.length - str.indexOf('.') - 1);
            str = (+str + 1 / mult);
        }
        return (negative ? "-" : "") + (str * 1).toFixedbackup(precision);
    };

    //some array haven't implement find
    if (!Array.prototype.find) {
        Array.prototype.find = function(callback, thisArg) {
            "use strict";
            var arr = this,
                arrLen = arr.length,
                i;
            for (i = 0; i < arrLen; i += 1) {
                if (callback.call(thisArg, arr[i], i, arr)) {
                    return arr[i];
                }
            }
            return undefined;
        };
    }
    /* end Global variable */

    module.exports = {
        setContentViewWithNewModuleView: (view, options) => {
            if (_this.contentView) {
                _this.contentView.remove();
            } else {
                $('body').append(`<div name="topView"></div>
                    <div name="contentView"></div>
                    <div name="bottomView"></div>`);
            }
            if (options && options.showTopBottomView) {
                if (!_this.topView) {
                    requirejs(['header'], View => {
                        _this.topView = new View();
                        $('body [name="topView"]').append(_this.topView.render().el);
                    })
                } else {
                    _this.topView.$el.show();
                    var link = window.location.hash;
                    if (link == "#job") {
                        _this.topView.$("h5[name='title']").html("Vacancy");
                    } else if (link == "#bookmark") {
                        _this.topView.$("h5[name='title']").html("Bookmark");
                    } else if (link == "#admin") {
                        _this.topView.$("h5[name='title']").html("Admin");
                    } else if (link == "#personalInfo") {
                        _this.topView.$("h5[name='title']").html("Account");
                    }


                    if (window.location.hash != "#job") {
                        _this.topView.$("a[name='goback'] > span").removeClass("fa-compass").addClass("fa-chevron-left");
                    } else {
                        _this.topView.$("a[name='goback'] > span").removeClass("fa-chevron-left").addClass("fa-compass");
                    }
                }


                if (!_this.bottomView) {
                    requirejs(['footer'], View => {
                        _this.bottomView = new View();
                        $('body [name="bottomView"]').append(_this.bottomView.render().el);
                    });
                } else {
                    _this.bottomView.$el.show();
                }
            } else {
                _this.topView && _this.topView.$el.hide();
                _this.bottomView && _this.bottomView.$el.hide();
            }
            _this.contentView = view;
            $('body [name="contentView"]').append(view.el);
            view.render();
        },
        formDataToJson: function(data) {
            return _.object(_.pluck(data, 'name'), _.pluck(data, 'value'));
        },
        getLastHash: function() {
            var length = window.location.hash.split('/').length;
            var getLasthSplitHashIndex = --length;
            return window.location.hash.split('/')[getLasthSplitHashIndex];
        },
    };
});