define(function(require, exports, module) {
    'use strict';

    var commonConfig = require('commonconfig');
    var Cookies = require('Cookies');

    Backbone.ajax = function() {
        var headers = arguments[0].headers;
        //     var useThisUrl = arguments[0].useThisUrl;
        if (arguments[0]) {
            var authorization = Cookies.get('Token');

            if (authorization) {
                arguments[0].headers = _.extend({}, headers, {
                    'Authorization': authorization
                });
            }
        }
        return Backbone.$.ajax.apply(Backbone.$, arguments);
    };

    // var execute = Backbone.Router.prototype.execute;

    // Backbone.Router.prototype.execute = function(callback, args, name) {
    //     execute.call(this, callback, args, name);
    // };

    // var remove = Backbone.View.prototype.remove;

    // Backbone.View.prototype.remove = function() {
    //     this.trigger('beforeRemove');
    //     remove.apply(this, arguments);
    //     this.isRemoved = true;
    //     this.trigger('afterRemove');
    // }
    module.exports = Backbone;
});