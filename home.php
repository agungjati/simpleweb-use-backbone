﻿<!doctype html>
<html lang="en">
<head>
    <title>Hello, world!</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet" >
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/tether.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body style="background:#f8f8f8;">
    <nav class="navbar fixed-top mb-4"  style="background:#fff; box-shadow:0 2px 3px #ccc">
        <div class="row">
            <div class="col col-3">
                <a class="navbar-brand" href="#!"><span class="fa fa-compass "></span></a>
            </div>
            <div class="col col-6 text-center align-text-bottom">
                <h5>Home</h5>
            </div>
           
            <div class="col col-3 text-center">
                <p class="text-gray-dark">Done</p>
            </div>
        </div>
    </nav>
    <nav class="navbar fixed-bottom " style="background:#fff; box-shadow:0 2px 3px #ccc">
        <div class="row">
            <div class="col col-3">
                <a class="col-auto navbar-brand text-gray-dark" href="#"><span class="fa fa-home fa-2x"></span></a>
            </div>
            <div class="col col-3">
                <a class="col-auto navbar-brand text-gray-dark" href="#"><span class="fa fa-book fa-2x"></span></a>
            </div>
            <div class="col col-3">
                <a class="col-auto navbar-brand text-gray-dark" href="#"><span class="fa fa-weixin fa-2x"></span></a>
            </div>
            <div class="col col-3">
                <a class="col-auto navbar-brand text-gray-dark" href="#"><span class="fa fa-user-circle-o fa-2x"></span></a>
            </div>
        </div>
       
    </nav>
    <br/>
   <div class="container text-muted" style="margin-top: 65px;margin-bottom: 75px;">
      <?php
	  if(isset($_GET['url'])=="FormExpertise")
	  {
	  }
	  else{
		include_once('listJobs.php');
	  }
	  ?> 
   </div>
    
    
   
</body>
</html>