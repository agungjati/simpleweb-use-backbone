﻿
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/tether.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body style="background:#f8f8f8;">
   <div class="container text-muted" style="margin-top: 65px;margin-bottom: 75px;">
       
   </div>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <script>
        $(document).ready(function () {
            $(document).on('click', '[data-name="list"] > div', function (e) {
                $('[data-name="detail"]', e.currentTarget).collapse('toggle');
            });
            $.ajax({
                method: "GET",
                url: "http://localhost:50981/Jobs",
                dataType: "json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "bearer " + getCookie("Token"));
                },
                success:function(data)
                {
                    var items = data.ResultList;
                    var txt = '';

                    for (let i = 0; i < items.length; i++)
                    {
                        txt += `  <a href="Congratulations.html?JobTitle=${items[i].JobTitle}&JobDescription=${items[i].JobDescription}&Address=${items[i].Address1} / ${items[i].Address2}&Latitude=${items[i].Latitude}&Longtitude=${items[i].Longitude}"><div class="gung text-muted row mt-3 ml-2 mr-2 bg-light p-4 " style="background:#fff; box-shadow:0 2px 3px #ccc; color:#ccc;">
           <div class="col-sm-auto ">
               <span class ="mt-2 fa text-muted fa-briefcase fa-3x" aria-hidden="true"></span>
           </div>
           <div class="col-sm-auto ml-4">
              <strong class ="text-muted">${items[i].JobTitle}</strong></a>
               <p>${items[i].JobDescription}</p>
           </div>
       </div></a>`;
                            
                    }
                    $(".container").html(txt);
                },
                error: function (xhr, msg) {
                    if (xhr.responseJSON && xhr.responseJSON.Message)
                        alert(xhr.responseJSON.Message);
                    else
                        alert(msg);
                }
            })

        });

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    </script>