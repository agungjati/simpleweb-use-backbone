// let pathLibs = '/simpleweb-use-backbone/js/';
let pathLibs = '../js/';
let pathLibsCustom = pathLibs + 'custom/';

require.config({
    paths: {
        'jquery': [
            'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min',
            'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min',
            pathLibs + 'jquery-3.2.1.min'
        ],
        'underscore': [
            'https://cdn.jsdelivr.net/underscorejs/1.8.3/underscore-min',
            'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min',
            pathLibs + 'underscore-1.8.3.min'
        ],
        'tether': [
            pathLibs + 'tether.min'
        ],
        'Cookies': pathLibs + 'js.cookie-2.1.3.min',
        'jquery.serialize.object': pathLibs + 'jquery.serialize.object-2.5.0.min',
        'deep-model': pathLibs + 'deep.model-0.10.4.min',
        'window.tether': pathLibsCustom + 'window.tether',
        'backbone.custom': pathLibsCustom + 'backbone.custom-1.3.3',
        'commonconfig': pathLibsCustom + 'commonconfig',
        'commonfunction': pathLibsCustom + 'commonfunction',
        'backbone.subroute': pathLibsCustom + 'backbone.subroute-0.4.6',
        'backbone': pathLibsCustom + 'backbone.override',
        'backbone.model': pathLibsCustom + 'backbone.model',
        'backbone.collection.paging': pathLibsCustom + 'backbone.collection.paging',
        'layoutmanager.original': [
            pathLibs + 'backbone.layoutmanager-1.0.0.min'
        ],
        'layoutmanager': [
            pathLibsCustom + 'backbone.layoutmanager.override'
        ],
        'require': 'https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.js',
        'text': pathLibs + 'requirejs.text-2.0.12.min',
        'bootstrap': pathLibs + 'bootstrap-v4.0.0-alpha.6.min',
        'eventaggregator': pathLibsCustom + 'eventaggregator',
        'subroute': pathLibsCustom + 'subroute/router',
        'header': pathLibsCustom + 'header/view',
        'footer': pathLibsCustom + 'footer/view'
    },
    shim: {
        jquery: {
            exports: 'jQuery'
        },
        'window.tether': {
            deps: ['jquery']
        },
        'tether': {
            deps: ['jquery']
        },
        commonfunction: {
            deps: ['jquery']
        },
        underscore: {
            exports: '_'
        },
        'text': {
            deps: ['require']
        },
        bootstrap: {
            deps: ['window.tether'],
            exports: '$.bootstrap'
        },
        'backbone.custom': {
            // deps: ['window.tether']
        },
        backbone: {
            deps: ['backbone.custom', 'bootstrap', 'underscore', 'text'],
            exports: 'Backbone'
        },
        'backbone.subroute': {
            deps: ['backbone']
        },
        'layoutmanager.original': {
            deps: ['backbone'],
            exports: 'LayoutManager'
        },
        'deep-model': {
            deps: ['underscore', 'backbone']
        },
        layoutmanager: {
            deps: ['layoutmanager.original']
        },
        header: {
            deps: ['layoutmanager']
        },
        footer: {
            deps: ['layoutmanager']
        }
    },
    callback: function(require) {
        requirejs(['main']);
    }
});