define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var Model = require('./model');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Cookies = require('Cookies');

    module.exports = LayoutManager.extend({
        className: 'container my-4',
        template: _.template(template),
        initialize: function() {
            this.model = new Model();
            this.listenTo(this.model, 'sync', function() {

            });
        },
        events: {
            'submit form': 'submit'
        },
        submit: function(event) {
            event.preventDefault();
            var formData = $(event.currentTarget).serializeArray();
            this.model.save(commonFunction.formDataToJson(formData))
        }
    });
});