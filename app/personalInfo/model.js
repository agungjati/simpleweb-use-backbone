define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');
    var Cookies = require('Cookies');

    var link = "";
    if (Cookies.get("roleName") == "Candidate") { link = "Candidates"; } else if (Cookies.get("roleName") == "Hiring manager") { link = "HiringManager"; }
    module.exports = Model.extend({
        urlRoot: "http://localhost:50981/api/" + link + "/" + Cookies.get("Token"),
        beforeInitialize: function() {
            this.on('sync', (model, response) => {
                alert(response.Message);
            })
        },
    });
});