//scripts/app
define(function(require, exports, module) {
    'use strict';

    let Backbone = require('backbone');
    let commonFunction = require('commonfunction');

    let fnSetContentView = function(pathViewFile, options) {
        require(['./' + pathViewFile + '/view'], function(View) {
            commonFunction.setContentViewWithNewModuleView(new View(), options);
        });
    };

    module.exports = Backbone.Router.extend({
        initialize: function() {
            this.app = {};
        },
        routes: {
            //'': 'showMainMenu',
            '': 'showLogin',
            'login': 'showLogin',
            'register': 'showRegister',
            'job': 'postJob',
            'jobAll': 'showJob',
            'job/:id': 'showJobDetail',
            'jobApply': 'showJobApply',
            'bookmark': 'showBookmark',
            'personalInfo': 'showpersonalInfo',
            'lostPassword': 'showlostPassword',
            'passReset': 'showpassReset',
            'admin': 'showAdmin',
            'admin/:id': 'showAdminDetail',
            '*actions': 'notFound'
        },
        start: () => {
            Backbone.history.start();
        },
        showMainMenu: () => {
            fnSetContentView('mainmenu');
        },
        showLogin: () => {
            fnSetContentView('./login');
        },
        showRegister: () => {
            fnSetContentView('./register');
        },
        showJob: () => {
            fnSetContentView('./Job/list', {
                showTopBottomView: true
            });
        },
        postJob: () => {
            fnSetContentView('./Job', {
                showTopBottomView: true
            });
        },
        showJobDetail: () => {
            fnSetContentView('./Job/detail', {
                showTopBottomView: true
            });
        },
        showJobApply: () => {
            fnSetContentView('./jobApply');
        },
        showBookmark: () => {
            fnSetContentView('./bookmark', {
                showTopBottomView: true
            });
        },
        showpersonalInfo: () => {
            fnSetContentView('./personalInfo', {
                showTopBottomView: true
            });
        },
        showlostPassword: () => {
            fnSetContentView('./lostPassword');
        },
        showpassReset: () => {
            fnSetContentView('./passReset');
        },
        showAdmin: () => {
            fnSetContentView('./admin', {
                showTopBottomView: true
            });
        },
        showAdminDetail: () => {
            fnSetContentView('./admin/detail', {
                showTopBottomView: true
            });
        },
        notFound: () => {
            alert('sorry not have what you want');
        }
    });
});