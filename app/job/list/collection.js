define(function(require, exports, module) {
    'use strict';
    var Collection = require('backbone.collection.paging');

    module.exports = Collection.extend({
        url: "http://localhost:50981/Jobs",
        parse: function(response) {
            if (response)
                return response.ResultList
            return response;
        }
    });
});