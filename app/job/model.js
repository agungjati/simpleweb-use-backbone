define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        urlRoot: '',
        defaults: {
            ResultList: {
                JobTitle: '',
                Address1: '',
                JobDescription: ''
            }
        }
    });
});