define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        urlRoot: 'http://localhost:50981/api/Jobs/',
        defaults: {
            ResultList: {
                JobTitle: '',
                Address1: '',
                JobDescription: ''
            }
        }
    });
});