define(function(require, exports, module) {
    'use strict';
    let LayoutManager = require('layoutmanager');
    var Model = require('./model');
    let template = require('text!./template.html');
    let Cookies = require('Cookies');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        template: _.template(template),
        useHeaderFooter: true,
        initialize: function() {
            this.model = new Model();
            this.listenTo(this.model, 'sync', () => {
                this.render()
            });
        },
        afterRender: function() {
            if (this.model.id)
                return;
            this.model.set('id', commonFunction.getLastHash());
            this.model.fetch();
            // $.ajax({
            //     method: "GET",
            //     url: "http://localhost:50981/api/Bookmark/"
            // });

            // function GetURLParameter() {
            //     var sPageURL = window.location.href;
            //     var indexOfLastSlash = sPageURL.lastIndexOf("/");

            //     if (indexOfLastSlash > 0 && sPageURL.length - 1 != indexOfLastSlash)
            //         return sPageURL.substring(indexOfLastSlash + 1);
            //     else
            //         return 0;
            // }

        },
        events: {
            'click #bookmark': 'bookmark'
        },
        bookmark: function(event) {
            $.ajax({
                method: "POST",
                url: "http://localhost:50981/api/Bookmark",
                data: "IsDisplay=true&JobID=" + GetURLParameter(),
                dataType: "json",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "bearer " + Cookies.get("Token"));
                },
                success: function(msg) {
                    alert("Bookmarked.");
                },
                error: function(xhr, msg) {
                    if (typeof xhr.responseJSON == 'object') {
                        let response = xhr.responseJSON
                        alert(response.Message || response.error_description || response.error);
                    } else
                        alert(msg.error);
                }
            });

            function GetURLParameter() {
                var sPageURL = window.location.href;
                var indexOfLastSlash = sPageURL.lastIndexOf("/");

                if (indexOfLastSlash > 0 && sPageURL.length - 1 != indexOfLastSlash)
                    return sPageURL.substring(indexOfLastSlash + 1);
                else
                    return 0;
            }
        }
    });


});