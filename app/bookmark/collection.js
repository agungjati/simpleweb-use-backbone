define(function(require, exports, module) {
    'use strict';
    var Collection = require('backbone.collection.paging');

    module.exports = Collection.extend({
        url: "http://localhost:50981/api/Bookmark/GetAll",
        parse: function(response) {
            return response;
        }
    });
});