define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');
    var Cookies = require('Cookies');

    module.exports = Model.extend({
        urlRoot: "http://localhost:50981/api/Account/ValidateForgot",
        beforeInitialize: function() {
            this.on('sync', (model, response) => {
                alert(response.Message);
            })
        }

    });
});