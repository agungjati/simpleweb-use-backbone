define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');
    var Cookies = require('Cookies');

    module.exports = Model.extend({
        urlRoot: "http://localhost:50981/Login/Token",
        beforeInitialize: function() {
            this.on('sync', (model, response) => {
                Cookies.set('Id', response.access_token);
                Cookies.set('Username', response.userName);
                Cookies.set('Token', `bearer ` + response.access_token);
                Cookies.set('roleName', response.roleName)
            })
        }

    });
});