define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var Model = require('./model');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        className: 'container my-4',
        template: _.template(template),
        initialize: function() {
            this.model = new Model();
            this.listenTo(this.model, 'sync', function() {
                window.location = "#passReset";
                $('button').removeAttr('disabled');
            });
            this.listenTo(this.model, 'request', function() {
                $('button').attr('disabled', 'disabled');
            });
            this.listenTo(this.model, 'error', function() {
                $('button').removeAttr('disabled');
            });
        },
        events: {
            'submit form': 'submit'
        },
        submit: function(event) {
            event.preventDefault();
            var formData = $(event.currentTarget).serializeArray();
            this.model.save(commonFunction.formDataToJson(formData));
        }
    });
});