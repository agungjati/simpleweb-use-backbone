define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'UserID',
        urlRoot: 'http://localhost:50981/api/Account/User',
        defaults: {
            FirstName: '',
            LastName: '',
            UserName: ''
        }
    });
});