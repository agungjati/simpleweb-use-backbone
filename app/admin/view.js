define(function(require, exports, module) {
    'use strict';
    let LayoutManager = require('layoutmanager');
    let Collection = require('./collection');
    let template = require('text!./template.html');
    let templateItem = require('text!./templateitem.html');
    let Cookies = require('Cookies');

    module.exports = LayoutManager.extend({
        template: _.template(template),
        userHeaderFooter: true,
        initialize: function() {
            this.collection = new Collection();
            this.once('afterRender', () => this.collection.fetch());

            this.collection.on('sync', (collection) => {
                this.render();
            });
        },
        afterRender: function() {
            _.each(this.collection.models, model => {
                let data = model.toJSON();
                var DOM = _.template(templateItem)(data);
                this.$('.container').append(DOM)
            })
        },
        events: {
            'click [name="delete"]': 'delete'
        },
        delete: function(e) {
            let deleted = window.confirm('delete it?');
            if (deleted) {
                let id = $(e.currentTarget).attr('value');
                debugger;
                $.ajax({
                    url: '',
                    type: 'DELETE'
                })
            }
        }
    });
});